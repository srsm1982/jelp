# What
jelp is an eclipse plugin to automatically create log statements in existing java code.

# Why
Because code base with poor logging is frustrating.
Because logs can be standardized. 

# Example

### Before
	public void someMethod(boolean condition) {
		Object object = getObject();
		for (int i = 0; i < 10; i++) {
			do {
				try {
					run();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} while (condition);
		}
	}
	
### After
	public void someMethod(boolean condition) {
		Object object = getObject();
		JALOG.debug("someMethod|assign|object|{}", object);
		for (int i = 0; i < 10; i++) {
			JALOG.debug("someMethod|for|i");
			do {
				JALOG.debug("someMethod|do|condition");
				try {
					run();
					JALOG.debug("someMethod|invoke|run|");
				} catch (Exception e) {
					JALOG.error("someMethod|exception|{}", e);
					e.printStackTrace();
				}
			} while (condition);
		}
	}

# Dependencies
The plugin assumes that the Java project is using [SLF4J](http://www.slf4j.org/).

# Usage
Right click on a project, and navigate to jelp menu.

# FAQ


# Development
This project is built using [Eclipse Tycho](https://www.eclipse.org/tycho/) and requires at least [maven 3.0](http://maven.apache.org/download.html) to be built via CLI. 

Clone and simply run :

    mvn install

In order to use the generated eclipse plugins in Eclipse, you will need [m2e](https://www.eclipse.org/m2e) 
and the [m2eclipse-tycho plugin](https://github.com/tesla/m2eclipse-tycho/). Update sites to install these plugins : 

# Future
(https://bitbucket.org/thodelu/jelp/issues)

