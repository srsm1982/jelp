package net.thodelu.jelp.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
* Sample integration test. In Eclipse, right-click > Run As > JUnit-Plugin. <br/>
* In Maven CLI, run "mvn integration-test".
*/
public class ActivatorTest {

	// @Test
	public void veryStupidTest() {
		assertEquals("jelp.core",Activator.PLUGIN_ID);
		assertTrue("Plugin should be started", Activator.getDefault().started);
	}
}