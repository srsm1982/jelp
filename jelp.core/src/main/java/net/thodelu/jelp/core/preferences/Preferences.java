package net.thodelu.jelp.core.preferences;

import org.eclipse.jface.preference.IPreferenceStore;

import net.thodelu.jelp.add.LogLevel;
import net.thodelu.jelp.Activator;

/**
 * Constant definitions for plug-in preferences
 */
public class Preferences {

	static final String loggerIdentifierKey = "loggerIdentifier";
	private String loggerIdentifier;

	static final String staticMethodLogLevelKey = "staticMethodLogLevel";
	static final String classMethodLogLevelKey = "classMethodLogLevel";
	static final String packageMethodLogLevelKey = "packageMethodLogLevel";
	static final String sourceCodeMethodLogLevelKey = "sourceCodeMethodLogLevel";
	static final String libraryMethodLogLevelKey = "libraryMethodLogLevel";

	private LogLevel classMethodLogLevel;
	private LogLevel packageMethodLogLevel;
	private LogLevel staticMethodLogLevel;
	private LogLevel sourceCodeMethodLogLevel;
	private LogLevel libraryMethodLogLevel;

	static final String ifLogLevelKey = "ifLogLevel";
	static final String switchLogLevelKey = "switchLogLevel";
	static final String forLogLevelKey = "forLogLevel";
	static final String whileLogLevelKey = "whileLogLevel";
	static final String catchLogLevelKey = "catchLogLevel";

	private LogLevel ifLogLevel;
	private LogLevel switchLogLevel;
	private LogLevel catchLogLevel;
	private LogLevel forLogLevel;
	private LogLevel whileLogLevel;

	/**
	 * Instantiates a new preferences.
	 */
	private Preferences() {
	}

	/**
	 * Gets the preferences.
	 *
	 * @return the preferences
	 */
	public static Preferences getPreferences() {

		Preferences prefs = new Preferences();
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();

		prefs.loggerIdentifier = store.getString(loggerIdentifierKey);

		prefs.classMethodLogLevel = LogLevel.valueOf(store.getString(classMethodLogLevelKey));
		prefs.packageMethodLogLevel = LogLevel.valueOf(store.getString(packageMethodLogLevelKey));
		prefs.staticMethodLogLevel = LogLevel.valueOf(store.getString(staticMethodLogLevelKey));
		prefs.sourceCodeMethodLogLevel = LogLevel.valueOf(store.getString(sourceCodeMethodLogLevelKey));
		prefs.libraryMethodLogLevel = LogLevel.valueOf(store.getString(libraryMethodLogLevelKey));

		prefs.ifLogLevel = LogLevel.valueOf(store.getString(ifLogLevelKey));
		prefs.switchLogLevel = LogLevel.valueOf(store.getString(switchLogLevelKey));
		prefs.catchLogLevel = LogLevel.valueOf(store.getString(catchLogLevelKey));
		prefs.forLogLevel = LogLevel.valueOf(store.getString(forLogLevelKey));
		prefs.whileLogLevel = LogLevel.valueOf(store.getString(whileLogLevelKey));

		return prefs;
	}

	public String getLoggerIdentifier() {
		return loggerIdentifier;
	}
	
	public LogLevel getStaticMethodLogLevel() {
		return staticMethodLogLevel;
	}

	public LogLevel getClassMethodLogLevel() {
		return classMethodLogLevel;
	}

	public LogLevel getPackageMethodLogLevel() {
		return packageMethodLogLevel;
	}

	public LogLevel getSourceCodeMethodLogLevel() {
		return sourceCodeMethodLogLevel;
	}

	public LogLevel getLibraryMethodLogLevel() {
		return libraryMethodLogLevel;
	}

	public LogLevel getIfLogLevel() {
		return ifLogLevel;
	}

	public LogLevel getSwitchLogLevel() {
		return switchLogLevel;
	}

	public LogLevel getForLogLevel() {
		return forLogLevel;
	}

	public LogLevel getWhileLogLevel() {
		return whileLogLevel;
	}
	
	public LogLevel getCatchLogLevel() {
		return catchLogLevel;
	}

}
