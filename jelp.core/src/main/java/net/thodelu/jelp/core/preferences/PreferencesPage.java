package net.thodelu.jelp.core.preferences;

import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import net.thodelu.jelp.Activator;

/**
 * The Class PreferencesPage.
 */
public class PreferencesPage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	/** The Constant validLogLevels. */
	private static final String[][] validLogLevels = new String[][] { { "NONE", "NONE" }, { "TRACE", "TRACE" },
			{ "DEBUG", "DEBUG" }, { "INFO", "INFO" }, { "WARN", "WARN" }, { "ERROR", "ERROR" } };

	/**
	 * Instantiates a new preferences page.
	 */
	public PreferencesPage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("jelp Preferences");
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {
		
		Composite parent = getFieldEditorParent();

		addField(new ComboFieldEditor(Preferences.staticMethodLogLevelKey, "Static Method Log Level", validLogLevels, parent));
		addField(new ComboFieldEditor(Preferences.classMethodLogLevelKey, "Class Method Log Level", validLogLevels, parent));
		addField(new ComboFieldEditor(Preferences.packageMethodLogLevelKey, "Package Method Log Level", validLogLevels, parent));
		addField(new ComboFieldEditor(Preferences.sourceCodeMethodLogLevelKey, "Source Method Log Level", validLogLevels, parent));
		addField(new ComboFieldEditor(Preferences.libraryMethodLogLevelKey, "Library Method Log Level", validLogLevels, parent));
		
		addField(new ComboFieldEditor(Preferences.ifLogLevelKey, "If-Else Log Level", validLogLevels, parent));
		addField(new ComboFieldEditor(Preferences.switchLogLevelKey, "Switch-Case Log Level", validLogLevels, parent));
		addField(new ComboFieldEditor(Preferences.forLogLevelKey, "For Loop Log Level", validLogLevels, parent));
		addField(new ComboFieldEditor(Preferences.whileLogLevelKey, "While Loop Log Level", validLogLevels, parent));
		addField(new ComboFieldEditor(Preferences.catchLogLevelKey, "Exception Log Level", validLogLevels, parent));
	}

}
