package net.thodelu.jelp.core.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import net.thodelu.jelp.Activator;

/**
 * Class used to initialize default preference values.
 */
public class Initializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();

		store.setDefault(Preferences.loggerIdentifierKey, "JELP");
		
		store.setDefault(Preferences.staticMethodLogLevelKey, "NONE");
		store.setDefault(Preferences.classMethodLogLevelKey, "TRACE");
		store.setDefault(Preferences.packageMethodLogLevelKey, "DEBUG");
		store.setDefault(Preferences.sourceCodeMethodLogLevelKey, "INFO");
		store.setDefault(Preferences.libraryMethodLogLevelKey, "DEBUG");
		
		store.setDefault(Preferences.ifLogLevelKey, "TRACE");
		store.setDefault(Preferences.switchLogLevelKey, "TRACE");
		store.setDefault(Preferences.forLogLevelKey, "TRACE");
		store.setDefault(Preferences.whileLogLevelKey, "TRACE");
		store.setDefault(Preferences.catchLogLevelKey, "ERROR");
		
	}

}
