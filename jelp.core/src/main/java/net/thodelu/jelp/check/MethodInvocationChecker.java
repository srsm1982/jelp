package net.thodelu.jelp.check;

import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Modifier;

import net.thodelu.jelp.add.LogLevel;
import net.thodelu.jelp.core.preferences.Preferences;

public class MethodInvocationChecker {
	
	public static LogLevel check(MethodInvocation node, String parentClass, Preferences prefs) {

		IMethodBinding binding = node.resolveMethodBinding();
		ITypeBinding calledClass = binding.getDeclaringClass();
		int modifiers = binding.getModifiers();

		String calledClassName = calledClass.getQualifiedName();
		String calledPackageName = calledClass.getPackage().getName();

		boolean isClassMethod = parentClass.equals(calledClassName);
		boolean isPackageMethod = parentClass.startsWith(calledPackageName);
		boolean isStaticMethod = Modifier.isStatic(modifiers);
		boolean isSourceMethod = calledClass.isFromSource();

		if (isStaticMethod) {
			return prefs.getStaticMethodLogLevel();

		} else if (isClassMethod) {
			return prefs.getClassMethodLogLevel();

		} else if (isPackageMethod) {
			return prefs.getPackageMethodLogLevel();
			
		} else if (isSourceMethod) {
			return prefs.getSourceCodeMethodLogLevel();
			
		} else {
			return prefs.getLibraryMethodLogLevel();
		}

	}

}
