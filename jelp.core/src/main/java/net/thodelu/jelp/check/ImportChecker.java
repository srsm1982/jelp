package net.thodelu.jelp.check;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ImportDeclaration;

/**
 * The Class ImportChecker.
 */
public class ImportChecker extends ASTVisitor {

	/** The has import. */
	public boolean hasImport = false;

	/* (non-Javadoc)
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.ImportDeclaration)
	 */
	@Override
	public boolean visit(ImportDeclaration node) {

		if ("org.slf4j.Logger".equals(node.getName().getFullyQualifiedName())) {
			hasImport = true;
		}

		return false;
	}

	/**
	 * Process.
	 *
	 * @param node the node
	 */
	public void process(ASTNode node) {
		node.accept(this);
	}

}