package net.thodelu.jelp.check;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import net.thodelu.jelp.add.LogLevel;
import net.thodelu.jelp.core.preferences.Preferences;

/**
 * The Class VariableDeclarationChecker.
 */
public class VariableDeclarationChecker extends ASTVisitor {

	/** The parent class. */
	final String parentClass;

	/** The variable name. */
	private String variableName;

	/** The level. */
	private LogLevel level = LogLevel.NONE;

	/** The prefs. */
	private Preferences prefs;

	/**
	 * Instantiates a new variable declaration checker.
	 *
	 * @param declaredClassName
	 *            the declared class name
	 */
	public VariableDeclarationChecker(String declaredClassName, Preferences prefs) {
		this.parentClass = declaredClassName;
		this.prefs = prefs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * SimpleName)
	 */
	@Override
	public boolean visit(SimpleName node) {

		ASTNode parent = node.getParent();

		// Check for variable casting.
		if (parent instanceof VariableDeclarationFragment) {
			this.variableName = node.getIdentifier();
		}
		
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * MethodInvocation)
	 */
	@Override
	public boolean visit(MethodInvocation node) {

		this.level = MethodInvocationChecker.check(node, parentClass, prefs);
		return false;
	}

	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public LogLevel getLevel() {
		return level;
	}

	/**
	 * Gets the variable name.
	 *
	 * @return the variable name
	 */
	public String getVariableName() {
		return variableName;
	}

	/**
	 * Process.
	 *
	 * @param node
	 *            the node
	 */
	public void process(ASTNode node) {
		node.accept(this);
	}

}