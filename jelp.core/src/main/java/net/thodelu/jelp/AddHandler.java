package net.thodelu.jelp;

import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import net.thodelu.jelp.handlers.PackageHandler;
import net.thodelu.jelp.handlers.ProjectHandler;
import net.thodelu.jelp.handlers.UnitHandler;

/**
 * AddHandler - Executed by click menu.<br/>
 */
public class AddHandler extends AbstractHandler {

	private final IWorkbenchWindow window;

	/**
	 * constructor.
	 */
	public AddHandler() {
		IWorkbench workbench = PlatformUI.getWorkbench();
		this.window = workbench.getActiveWorkbenchWindow();
	}
	
	

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		if (currentSelection != null) {
			if (currentSelection instanceof IStructuredSelection) {
				IStructuredSelection selections = (IStructuredSelection) currentSelection;
				Iterator iterator = selections.iterator();
				while (iterator.hasNext()) {
					Object selection = iterator.next();
					if (selection instanceof IJavaProject) {
						IJavaProject project = (IJavaProject) selection;
						ProjectHandler.getInstance().handle(project);
					} 
					else if (selection instanceof IPackageFragment){
						IPackageFragment packageFragment = (IPackageFragment) selection;
						PackageHandler.getInstance().handle(packageFragment);
					}
					else if (selection instanceof ICompilationUnit){
						ICompilationUnit javaUnit = (ICompilationUnit) selection;
						UnitHandler.getInstance().handle(javaUnit);
					}
				}
			}
		}
		MessageDialog.openInformation(window.getShell(), Activator.PLUGIN_ID, "Done!");
		return null;
	}

}
