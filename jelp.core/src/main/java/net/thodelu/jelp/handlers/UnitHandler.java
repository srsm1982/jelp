package net.thodelu.jelp.handlers;

import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.filebuffers.ITextFileBufferManager;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentRewriteSession;
import org.eclipse.jface.text.DocumentRewriteSessionType;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentExtension4;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.TextEdit;

import net.thodelu.jelp.Activator;
import net.thodelu.jelp.check.ImportChecker;
import net.thodelu.jelp.core.preferences.Preferences;
import net.thodelu.jelp.edit.FieldAdder;
import net.thodelu.jelp.edit.ImportEditor;
import net.thodelu.jelp.edit.UnitEditor;
import net.thodelu.jelp.scrub.UnitScrubber;

/**
 * The Class JavaUnitHandler.
 */
public class UnitHandler {

	private static final UnitHandler instance = new UnitHandler();

	private UnitHandler() {
	}

	/**
	 * Gets the single instance of JavaUnitHandler.
	 *
	 * @return single instance of JavaUnitHandler
	 */
	public static UnitHandler getInstance() {
		return instance;
	}

	/**
	 * Handle.
	 *
	 * @param javaUnit
	 *            the java unit
	 */
	public void handle(ICompilationUnit javaUnit) {
		try {
			Preferences prefs = Preferences.getPreferences();
			scrub(javaUnit, prefs);
			addLog(javaUnit, prefs);

		} catch (Exception e) {
			String element = javaUnit.getElementName();
			Activator.getDefault().getLog()
					.log(new Status(Status.ERROR, Activator.PLUGIN_ID, "UnitHandler failed to process:" + element, e));
		}
	}

	/**
	 * Scrub.
	 *
	 * @param javaUnit
	 *            the java unit
	 * @param prefs
	 * @throws CoreException
	 *             the core exception
	 * @throws MalformedTreeException
	 *             the malformed tree exception
	 * @throws BadLocationException
	 *             the bad location exception
	 */
	@SuppressWarnings("deprecation")
	private void scrub(ICompilationUnit javaUnit, Preferences prefs)
			throws CoreException, MalformedTreeException, BadLocationException {

		ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(javaUnit);
		parser.setResolveBindings(true);

		ASTNode astNode = parser.createAST(null);
		CompilationUnit compileUnit = (CompilationUnit) astNode;

		AST ast = compileUnit.getAST();
		ASTRewrite astrw = ASTRewrite.create(ast);

		UnitScrubber scrubber = new UnitScrubber(astrw, prefs);
		scrubber.process(compileUnit);

		ITextFileBufferManager bufferManager = FileBuffers.getTextFileBufferManager();
		IPath path = javaUnit.getPath();
		bufferManager.connect(path, null);

		ITextFileBuffer textFileBuffer = bufferManager.getTextFileBuffer(path);
		IDocument document = textFileBuffer.getDocument();

		if (document instanceof IDocumentExtension4) {

			IDocumentExtension4 doc4 = (IDocumentExtension4) document;
			DocumentRewriteSession rewriteSession = doc4.startRewriteSession(DocumentRewriteSessionType.SEQUENTIAL);
			TextEdit textEdits = astrw.rewriteAST();
			textEdits.apply(document);
			doc4.stopRewriteSession(rewriteSession);
		}

		textFileBuffer.commit(null, false);
	}

	/**
	 * Auto log.
	 *
	 * @param javaUnit
	 *            the java unit
	 * @param prefs
	 * @throws CoreException
	 *             the core exception
	 * @throws MalformedTreeException
	 *             the malformed tree exception
	 * @throws BadLocationException
	 *             the bad location exception
	 */
	@SuppressWarnings("deprecation")
	private void addLog(ICompilationUnit javaUnit, Preferences prefs)
			throws CoreException, MalformedTreeException, BadLocationException {

		ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(javaUnit);
		parser.setResolveBindings(true);

		ASTNode astNode = parser.createAST(null);
		CompilationUnit compileUnit = (CompilationUnit) astNode;

		AST ast = compileUnit.getAST();
		ASTRewrite astrw = ASTRewrite.create(ast);

		UnitEditor unitEditor = new UnitEditor(astrw, prefs);
		unitEditor.process(compileUnit);
		
		IPath path = javaUnit.getPath();
		ITextFileBufferManager bufferManager = FileBuffers.getTextFileBufferManager();

		bufferManager.connect(path, null);
		ITextFileBuffer textFileBuffer = bufferManager.getTextFileBuffer(path);
		IDocument document = textFileBuffer.getDocument();
		
		TextEdit codeEdits = astrw.rewriteAST(document, null);
		if(codeEdits.getChildrenSize() == 0) {
			return;
		}
		
		ImportChecker checker = new ImportChecker();
		checker.process(compileUnit);

		if (!checker.hasImport) {
			ImportEditor importEditor = new ImportEditor();
			importEditor.addImports(compileUnit, ast, astrw);
		}
		
		FieldAdder adder = new FieldAdder(astrw, prefs);
		adder.process(compileUnit);

		if (document instanceof IDocumentExtension4) {

			IDocumentExtension4 doc4 = (IDocumentExtension4) document;
			DocumentRewriteSession rewriteSession = doc4.startRewriteSession(DocumentRewriteSessionType.SEQUENTIAL);

			MultiTextEdit multiEdit = new MultiTextEdit();
			TextEdit allEdits = astrw.rewriteAST(document, null);
			for (TextEdit textEdit : allEdits.getChildren()) {
				multiEdit.addChild(textEdit.copy());
			}
			multiEdit.apply(document, MultiTextEdit.CREATE_UNDO);

			doc4.stopRewriteSession(rewriteSession);
		
		} else {
		
			// TODO
		}

		textFileBuffer.commit(null, false);
	}

}
