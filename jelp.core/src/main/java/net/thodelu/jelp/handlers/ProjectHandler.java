package net.thodelu.jelp.handlers;

import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

import net.thodelu.jelp.Activator;

/**
 * The Class JavaProjectHandler.
 */
public class ProjectHandler {

	private static final ProjectHandler instance = new ProjectHandler();

	/**
	 * Instantiates a new java project handler.
	 */
	private ProjectHandler() {
	}

	/**
	 * Gets the single instance of JavaProjectHandler.
	 *
	 * @return single instance of JavaProjectHandler
	 */
	public static ProjectHandler getInstance() {
		return instance;
	}

	/**
	 * Handle.
	 *
	 * @param project
	 *            the project
	 */
	public void handle(IJavaProject project) {
		try {
			IPackageFragment[] packageFragments = project.getPackageFragments();
			if (packageFragments != null) {
				for (IPackageFragment packageFragment : packageFragments) {
					if (packageFragment != null) {
						PackageHandler.getInstance().handle(packageFragment);
					}
				}
			}
		} catch (JavaModelException e) {
			Activator.getDefault().getLog().log(
					new Status(Status.ERROR, Activator.PLUGIN_ID, "ProjectHandler failed to process:" + project, e));
		}
	}

}
