package net.thodelu.jelp.handlers;

import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

import net.thodelu.jelp.Activator;

public class PackageHandler {

	private static final PackageHandler instance = new PackageHandler();

	/**
	 * Instantiates a new java project handler.
	 */
	private PackageHandler() {
	}

	/**
	 * Gets the single instance of PackageHandler.
	 *
	 * @return single instance of PackageHandler
	 */
	public static PackageHandler getInstance() {
		return instance;
	}

	/**
	 * Handle.
	 *
	 * @param packageFragment the package fragment
	 */
	public void handle(IPackageFragment packageFragment) {
		try {
			ICompilationUnit[] compilationUnits = packageFragment.getCompilationUnits();
			if (compilationUnits != null) {
				for (ICompilationUnit javaUnit : compilationUnits) {
					UnitHandler.getInstance().handle(javaUnit);
				}
			}
		} catch (JavaModelException e) {
			Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID,
					"PackageHandler failed to process:" + packageFragment, e));
		}
	}

}
