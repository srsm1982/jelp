package net.thodelu.jelp.edit;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.ChildListPropertyDescriptor;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jdt.core.dom.rewrite.ListRewrite;

import net.thodelu.jelp.add.LogCreator;
import net.thodelu.jelp.add.LogLevel;
import net.thodelu.jelp.check.MethodInvocationChecker;
import net.thodelu.jelp.check.VariableDeclarationChecker;
import net.thodelu.jelp.core.preferences.Preferences;

/**
 * The Class MethodEditor.
 */
public class MethodEditor extends ASTVisitor {

	/** The parent class. */
	private final String parentClass;

	/** The method name. */
	private final String methodName;

	/** The astrw. */
	private final ASTRewrite astrw;

	/** The prefs. */
	private final Preferences prefs;

	/**
	 * Instantiates a new method editor.
	 *
	 * @param parentClass
	 *            the parent class
	 * @param methodName
	 *            the method name
	 * @param astrw
	 *            the astrw
	 * @param bodyBlock
	 *            the body block
	 */
	public MethodEditor(String parentClass, String methodName, ASTRewrite astrw, Preferences prefs) {
		this.parentClass = parentClass;
		this.methodName = methodName;
		this.astrw = astrw;
		this.prefs = prefs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * VariableDeclarationStatement)
	 */
	@Override
	public boolean visit(VariableDeclarationStatement node) {
		
		VariableDeclarationChecker checker = new VariableDeclarationChecker(parentClass, prefs);
		checker.process(node);

		LogLevel level = checker.getLevel();
		String variableName = checker.getVariableName();

		if (!level.isNone()) {

			AST ast = node.getAST();
			Statement insert = LogCreator.createLog(ast, prefs.getLoggerIdentifier(), level, methodName, "assign",
					variableName + "|{}", variableName);
			
			ASTNode block = node.getParent();
			ListRewrite lrw = astrw.getListRewrite(block, Block.STATEMENTS_PROPERTY);
			lrw.insertAfter(insert, node, null);
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * IfStatement)
	 */
	@Override
	public boolean visit(IfStatement node) {

		LogLevel level = prefs.getIfLogLevel();
		if (!level.isNone()) {
			
			String expression = node.getExpression().toString();

			Statement thenStatement = node.getThenStatement();
			if (thenStatement instanceof Block) {

				AST ast = node.getAST();
				Statement insert = LogCreator.createLog(ast, prefs.getLoggerIdentifier(), level, methodName, "if",
						expression + "|true");

				Block thenBlock = (Block) thenStatement;
				ListRewrite lrw = astrw.getListRewrite(thenBlock, Block.STATEMENTS_PROPERTY);
				lrw.insertFirst(insert, null);
			}

			Statement elseStatement = node.getElseStatement();
			if (elseStatement != null && elseStatement instanceof Block) {

				AST ast = node.getAST();
				Statement insert = LogCreator.createLog(ast, prefs.getLoggerIdentifier(), level, methodName, "if",
						expression + "|false");

				Block elseBlock = (Block) elseStatement;
				ListRewrite lrw = astrw.getListRewrite(elseBlock, Block.STATEMENTS_PROPERTY);
				lrw.insertFirst(insert, null);
			}
		}

		return super.visit(node);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * SwitchCase)
	 */
	@Override
	public boolean visit(SwitchCase node) {

		LogLevel level = prefs.getSwitchLogLevel();
		if (!level.isNone()) {
			
			SwitchStatement parent = (SwitchStatement) node.getParent();
			String expression = node.isDefault() ? "default" : node.getExpression().toString();
			String condition = parent.getExpression().toString();

			AST ast = node.getAST();
			Statement insert = LogCreator.createLog(ast, prefs.getLoggerIdentifier(), level, methodName, "switch",
					condition + "|" + expression);

			ListRewrite lrw = astrw.getListRewrite(parent, SwitchStatement.STATEMENTS_PROPERTY);
			lrw.insertAfter(insert, node, null);
		}
		return false;
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean visit(MethodInvocation node) {

		ASTNode parent = node.getParent();
		if (parent instanceof ExpressionStatement) {

			ASTNode block = parent.getParent();
			if (block instanceof Block || block instanceof SwitchStatement) {

				LogLevel level = MethodInvocationChecker.check(node, parentClass, prefs);
				if (!level.isNone()) {

					String calledMethodName = node.getName().getIdentifier();

					StringBuffer buf = new StringBuffer(calledMethodName + "|");
					List<Object> arguments = node.arguments();
					List<String> simpleArgs = new ArrayList<String>(arguments.size());
					
					String delim = "";
					for (Object object : arguments) {
						buf.append(delim);
						if (object instanceof SimpleName) {
							SimpleName name = (SimpleName) object;
							buf.append("{}");
							simpleArgs.add(name.getIdentifier());
						} else {
							buf.append(String.valueOf(object));
						}
						delim = ",";
					}

					AST ast = node.getAST();
					Statement insert = LogCreator.createLog(ast, prefs.getLoggerIdentifier(), level, methodName, "invoke",
							buf.toString(), simpleArgs);

					ChildListPropertyDescriptor statementsProperty = (block instanceof Block)
							? Block.STATEMENTS_PROPERTY : SwitchStatement.STATEMENTS_PROPERTY;
					ListRewrite lrw = astrw.getListRewrite(block, statementsProperty);
					lrw.insertAfter(insert, parent, null);
				}
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * CatchClause)
	 */
	@Override
	public boolean visit(CatchClause node) {

		LogLevel level = prefs.getCatchLogLevel();
		if (!level.isNone()) {

			SingleVariableDeclaration exception = node.getException();
			String exceptionName = exception.getName().getIdentifier();

			AST ast = node.getAST();
			Statement insert = LogCreator.createLog(ast, prefs.getLoggerIdentifier(), level, methodName, "exception", "{}",
					exceptionName);

			ASTNode block = node.getBody();
			ChildListPropertyDescriptor statementsProperty = Block.STATEMENTS_PROPERTY;

			ListRewrite lrw = astrw.getListRewrite(block, statementsProperty);
			lrw.insertFirst(insert, null);
		}
		return super.visit(node);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * ForStatement)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean visit(ForStatement node) {

		LogLevel level = prefs.getForLogLevel();
		if (!level.isNone()) {

			Statement body = node.getBody();
			if (body instanceof Block) {

				List initializers = node.initializers();
				if (initializers != null && !initializers.isEmpty()) {

					Object initializer = initializers.get(0);
					if (initializer != null && initializer instanceof VariableDeclarationExpression) {

						VariableDeclarationExpression declaration = (VariableDeclarationExpression) initializer;
						VariableDeclarationFragment fragment = (VariableDeclarationFragment) declaration.fragments()
								.get(0);

						String loopIdentifier = fragment.getName().getIdentifier();

						AST ast = node.getAST();
						Statement insert = LogCreator.createLog(ast, prefs.getLoggerIdentifier(), level, methodName, "for",
								loopIdentifier);

						ASTNode block = node.getBody();
						ChildListPropertyDescriptor statementsProperty = Block.STATEMENTS_PROPERTY;

						ListRewrite lrw = astrw.getListRewrite(block, statementsProperty);
						lrw.insertFirst(insert, null);
					}
				}
			}
		}
		return super.visit(node);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * EnhancedForStatement)
	 */
	@Override
	public boolean visit(EnhancedForStatement node) {

		LogLevel level = prefs.getForLogLevel();
		if (!level.isNone()) {

			Statement body = node.getBody();
			if (body instanceof Block) {

				SingleVariableDeclaration parameter = node.getParameter();
				String loopIdentifier = parameter.getName().getIdentifier();

				AST ast = node.getAST();
				Statement insert = LogCreator.createLog(ast, prefs.getLoggerIdentifier(), level, methodName, "for",
						loopIdentifier);

				ASTNode block = node.getBody();
				ChildListPropertyDescriptor statementsProperty = Block.STATEMENTS_PROPERTY;

				ListRewrite lrw = astrw.getListRewrite(block, statementsProperty);
				lrw.insertFirst(insert, null);

			}
		}
		return super.visit(node);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * WhileStatement)
	 */
	@Override
	public boolean visit(WhileStatement node) {

		LogLevel level = prefs.getWhileLogLevel();
		if (!level.isNone()) {

			Statement body = node.getBody();
			if (body instanceof Block) {

				String loopIdentifier = String.valueOf(node.getExpression());

				AST ast = node.getAST();
				Statement insert = LogCreator.createLog(ast, prefs.getLoggerIdentifier(), level, methodName, "while",
						loopIdentifier);

				ASTNode block = node.getBody();
				ChildListPropertyDescriptor statementsProperty = Block.STATEMENTS_PROPERTY;

				ListRewrite lrw = astrw.getListRewrite(block, statementsProperty);
				lrw.insertFirst(insert, null);
			}
		}

		return super.visit(node);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * DoStatement)
	 */
	@Override
	public boolean visit(DoStatement node) {

		LogLevel level = prefs.getWhileLogLevel();
		if (!level.isNone()) {
			
			Statement body = node.getBody();
			if (body instanceof Block) {

				String loopIdentifier = String.valueOf(node.getExpression());

				AST ast = node.getAST();
				Statement insert = LogCreator.createLog(ast, prefs.getLoggerIdentifier(), level, methodName, "do",
						loopIdentifier);

				ASTNode block = node.getBody();
				ChildListPropertyDescriptor statementsProperty = Block.STATEMENTS_PROPERTY;

				ListRewrite lrw = astrw.getListRewrite(block, statementsProperty);
				lrw.insertFirst(insert, null);
			}
		}

		return super.visit(node);
	}

	/**
	 * Process.
	 *
	 * @param node
	 *            the node
	 */
	public void process(ASTNode node) {
		node.accept(this);
	}

}