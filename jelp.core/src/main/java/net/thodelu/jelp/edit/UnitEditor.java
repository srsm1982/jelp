package net.thodelu.jelp.edit;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;

import net.thodelu.jelp.core.preferences.Preferences;

/**
 * The Class UnitEditor.
 */
public class UnitEditor extends ASTVisitor {

	/** The rewriter. */
	ASTRewrite astrw;

	/** The prefs. */
	Preferences prefs;
	
	/** Fully qualified parent class name. */
	String parentQualifiedName;

	/**
	 * Instantiates a new unit editor.
	 *
	 * @param astrw
	 *            the astrw
	 */
	public UnitEditor(ASTRewrite astrw, Preferences prefs) {
		this.astrw = astrw;
		this.prefs = prefs;
	}
	
	@Override
	public boolean visit(TypeDeclaration node) {

		// Set parentClass name
		ITypeBinding typeDeclaration = node.resolveBinding().getTypeDeclaration();
		parentQualifiedName = typeDeclaration.getQualifiedName();

		return super.visit(node);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * MethodDeclaration)
	 */
	@Override
	public boolean visit(MethodDeclaration node) {

		String methodName = node.getName().getIdentifier();

		// Delegate
		MethodEditor methodEditor = new MethodEditor(parentQualifiedName, methodName, astrw, prefs);
		methodEditor.process(node);

		return super.visit(node);
	}

	/**
	 * Process.
	 *
	 * @param node
	 *            the node
	 */
	public void process(ASTNode node) {
		node.accept(this);
	}

}