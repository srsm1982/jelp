package net.thodelu.jelp.edit;

import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.TypeLiteral;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jdt.core.dom.rewrite.ListRewrite;

import net.thodelu.jelp.core.preferences.Preferences;

public class FieldAdder extends ASTVisitor {
	
	/** The rewriter. */
	ASTRewrite astrw;

	/** The prefs. */
	Preferences prefs;
	
	/**
	 * Instantiates a new unit editor.
	 *
	 * @param astrw
	 *            the astrw
	 */
	public FieldAdder(ASTRewrite astrw, Preferences prefs) {
		this.astrw = astrw;
		this.prefs = prefs;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * TypeDeclaration)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean visit(TypeDeclaration node) {

		// Set parentClass name
		ITypeBinding typeDeclaration = node.resolveBinding().getTypeDeclaration();
		String parentSimpleName = typeDeclaration.getName();

		boolean alreadySet = false;

		FieldDeclaration[] fields = node.getFields();
		if (fields != null) {
			for (FieldDeclaration field : fields) {
				List fragments = field.fragments();
				for (Object fragment : fragments) {
					if (fragment instanceof VariableDeclarationFragment) {
						VariableDeclarationFragment vFragment = (VariableDeclarationFragment) fragment;
						if (prefs.getLoggerIdentifier().equals(vFragment.getName().getIdentifier())) {
							alreadySet = true;
							break;
						}
					}
				}
				if (alreadySet) {
					break;
				}
			}
		}

		// Insert field
		if (!alreadySet) {
			insertField(node, parentSimpleName);
		}

		return super.visit(node);
	}

	/**
	 * Creates : private static final Logger logger =
	 * LoggerFactory.getLogger(NAME.class);
	 *
	 * @param node
	 *            the node
	 * @param parentSimpleName
	 *            the parent simple name
	 */
	@SuppressWarnings("unchecked")
	private void insertField(TypeDeclaration node, String parentSimpleName) {

		// Get ast
		AST ast = node.getAST();

		// NAME.class
		TypeLiteral newTypeLiteral = ast.newTypeLiteral();
		newTypeLiteral.setType(ast.newSimpleType(ast.newSimpleName(parentSimpleName)));

		// LoggerFactory.getLogger(NAME.class)
		MethodInvocation initializer = ast.newMethodInvocation();
		initializer.setExpression(ast.newSimpleName("LoggerFactory"));
		initializer.setName(ast.newSimpleName("getLogger"));
		initializer.arguments().add(newTypeLiteral);

		// SOGGER =
		VariableDeclarationFragment fragment = ast.newVariableDeclarationFragment();
		fragment.setName(ast.newSimpleName(prefs.getLoggerIdentifier()));
		fragment.setInitializer(initializer);

		// private static final Logger SOGGER =
		// LoggerFactory.getLogger(NAME.class)
		FieldDeclaration newField = ast.newFieldDeclaration(fragment);
		newField.setType(ast.newSimpleType(ast.newSimpleName("Logger")));
		newField.modifiers().add(ast.newModifier(ModifierKeyword.PRIVATE_KEYWORD));
		newField.modifiers().add(ast.newModifier(ModifierKeyword.STATIC_KEYWORD));
		newField.modifiers().add(ast.newModifier(ModifierKeyword.FINAL_KEYWORD));

		// Update
		ListRewrite lrw = astrw.getListRewrite(node, TypeDeclaration.BODY_DECLARATIONS_PROPERTY);
		lrw.insertFirst(newField, null);
	}
	
	/**
	 * Process.
	 *
	 * @param node
	 *            the node
	 */
	public void process(ASTNode node) {
		node.accept(this);
	}
	

}
