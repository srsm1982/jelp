package net.thodelu.jelp.edit;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ImportDeclaration;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jdt.core.dom.rewrite.ListRewrite;

/**
 * The Class ImportAdder.
 */
public class ImportEditor {

	/**
	 * Adds the.
	 *
	 * @param compileUnit the compile unit
	 * @param ast the ast
	 * @param astrw the astrw
	 */
	public void addImports(CompilationUnit compileUnit, AST ast, ASTRewrite astrw) {
		ListRewrite lrw = astrw.getListRewrite(compileUnit, CompilationUnit.IMPORTS_PROPERTY);

		ImportDeclaration loggerImport = ast.newImportDeclaration();
		loggerImport.setName(ast.newName(new String[] { "org", "slf4j", "Logger" }));
		lrw.insertLast(loggerImport, null);

		ImportDeclaration factoryImport = ast.newImportDeclaration();
		factoryImport.setName(ast.newName(new String[] { "org", "slf4j", "LoggerFactory" }));
		lrw.insertLast(factoryImport, null);
	}
}
