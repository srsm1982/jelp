package net.thodelu.jelp.scrub;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;

import net.thodelu.jelp.core.preferences.Preferences;

/**
 * The Class UnitScrubber.
 */
public class UnitScrubber extends ASTVisitor {

	/** The rewriter. */
	ASTRewrite astrw;
	
	/** The prefs. */
	Preferences prefs;

	/**
	 * Instantiates a new unit scrubber.
	 *
	 * @param astrw the astrw
	 */
	public UnitScrubber(ASTRewrite astrw, Preferences prefs) {
		this.astrw = astrw;
		this.prefs = prefs;
	}
	
	// TODO Remove imports
	// TODO Remove field

	/* (non-Javadoc)
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.MethodDeclaration)
	 */
	@Override
	public boolean visit(MethodDeclaration node) {
		
		// Delegate
		MethodScrubber scrubber = new MethodScrubber(astrw, prefs);
		scrubber.process(node);
		
		return super.visit(node);
	}

	/**
	 * Process.
	 *
	 * @param node the node
	 */
	public void process(ASTNode node) {
		node.accept(this);
	}

}
