package net.thodelu.jelp.scrub;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;

import net.thodelu.jelp.core.preferences.Preferences;

/**
 * The Class MethodScrubber.
 */
public class MethodScrubber extends ASTVisitor {

	/** The astrw. */
	private final ASTRewrite astrw;
	
	/** The prefs. */
	private final Preferences prefs;

	/**
	 * Instantiates a new method scrubber.
	 *
	 * @param astrw the astrw
	 */
	public MethodScrubber(ASTRewrite astrw, Preferences prefs) {
		this.astrw = astrw;
		this.prefs = prefs;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.ExpressionStatement)
	 */
	@Override
	public boolean visit(ExpressionStatement node) {
		
		Expression expression = node.getExpression();
		if(expression instanceof MethodInvocation){
			MethodInvocation methodNode = (MethodInvocation) expression;
			Expression methodExpression = methodNode.getExpression();
			if (methodExpression instanceof SimpleName) {
				SimpleName name = (SimpleName) methodExpression;
				if (prefs.getLoggerIdentifier().equals(name.getIdentifier())) {
					astrw.remove(node, null);
				}
			}
		}
		
		return false;
	}

	/**
	 * Process.
	 *
	 * @param node the node
	 */
	public void process(ASTNode node) {
		node.accept(this);
	}

}