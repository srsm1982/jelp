package net.thodelu.jelp.add;

/**
 * The Enum LogLevel.
 */
public enum LogLevel {

	/**
	 * The none - not meant to be used, but in case it is, method will be trace.
	 */
	NONE("trace"),

	/** The trace. */
	TRACE("trace"),

	/** The debug. */
	DEBUG("debug"),

	/** The info. */
	INFO("info"),

	/** The warn. */
	WARN("warn"),

	/** The error. */
	ERROR("error");

	/** The method. */
	final String method;

	/**
	 * Instantiates a new log level.
	 *
	 * @param method
	 *            the method
	 */
	private LogLevel(String method) {
		this.method = method;
	}

	/**
	 * Gets the method.
	 *
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}
	
	/**
	 * Checks if is none.
	 *
	 * @return true, if is none
	 */
	public boolean isNone(){
		return this == LogLevel.NONE;
	}
	
}
