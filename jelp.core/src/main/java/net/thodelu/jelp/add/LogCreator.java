package net.thodelu.jelp.add;

import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.StringLiteral;

public class LogCreator {
	
	@SuppressWarnings("unchecked")
	public static Statement createLog(AST ast, String loggerIdentifier, LogLevel level, String methodName, String verb,
			String message) {

		StringLiteral newLiteral = ast.newStringLiteral();
		newLiteral.setLiteralValue(methodName + "|" + verb + "|" + message);

		MethodInvocation newInvocation = ast.newMethodInvocation();
		newInvocation.setExpression(ast.newSimpleName(loggerIdentifier));
		newInvocation.setName(ast.newSimpleName(level.getMethod()));
		newInvocation.arguments().add(newLiteral);

		return ast.newExpressionStatement(newInvocation);
	}


	@SuppressWarnings("unchecked")
	public static Statement createLog(AST ast, String loggerIdentifier, LogLevel level, String methodName, String verb,
			String message, String variableName) {

		StringLiteral newLiteral = ast.newStringLiteral();
		newLiteral.setLiteralValue(methodName + "|" + verb + "|" + message);

		MethodInvocation newInvocation = ast.newMethodInvocation();
		newInvocation.setExpression(ast.newSimpleName(loggerIdentifier));
		newInvocation.setName(ast.newSimpleName(level.getMethod()));
		newInvocation.arguments().add(newLiteral);
		newInvocation.arguments().add(ast.newSimpleName(variableName));

		return ast.newExpressionStatement(newInvocation);
	}

	@SuppressWarnings("unchecked")
	public static Statement createLog(AST ast, String loggerIdentifier, LogLevel level, String methodName, String verb,
			String message, List<String> simpleArgs) {

		StringLiteral newLiteral = ast.newStringLiteral();
		newLiteral.setLiteralValue(methodName + "|" + verb + "|" + message);

		MethodInvocation newInvocation = ast.newMethodInvocation();
		newInvocation.setExpression(ast.newSimpleName(loggerIdentifier));
		newInvocation.setName(ast.newSimpleName(level.getMethod()));
		newInvocation.arguments().add(newLiteral);
		for (String simpleArg : simpleArgs) {
			newInvocation.arguments().add(ast.newSimpleName(simpleArg));
		}

		return ast.newExpressionStatement(newInvocation);
	}

}
